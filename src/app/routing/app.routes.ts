import {Routes} from '@angular/router';
import {NotFoundComponent} from '../components/not-found/not-found.component';
import {AuthGuard} from './guards/auth.guard';

export const APP_ROUTES: Routes = [
  {
    path: 'session',
    loadChildren: '../modules/session/session.module#SessionModule',
  },
  {
    path: 'users',
    loadChildren: '../modules/dashboard/dashboard.module#DashboardModule',
    //canLoad: [AuthGuard]
  },
  {
    path: '',
    redirectTo: '/session/login',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];
