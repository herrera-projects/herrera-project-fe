import {HttpClient} from '@angular/common/http';
import {Injector} from '@angular/core';

export abstract class HttpService  {

  protected abstract injector(): Injector;

  public httpClient(): HttpClient {
    return this.injector().get(HttpClient);
  }

}
