import {environment} from "../../../environments/environment";


export class HttpConstants {

  public static host = environment.api.host;

  public static _queryEncoder = '?';

  public static _queryUnion = '&';

  public static _urls = {
    page: 'page=',
    per_page: 'per_page=',
    login: '/api/login',
    users: '/api/users',
    resources: '/api/unknown' +
    '\n'
  };

  public static _UrlsImageDefault = {
    userImageDefault: 'assets/images/loadAvatar.jpg'
  };
  public static _UrlsImageDefaultEnrrollment = {
    userImageDefault: 'assets/images/dummyAvatar.png'
  };
  public static _UrlsImageDefaultPortal = {
    userImageDefault: 'assets/images/loadAvatar.jpg'
  };

  public static getApiAddress() {
    return this.host
  }

  public static getPath(): string {
    return this._urls.login;
  }

  public static getUrl(): string {
    return this.getApiAddress() + this._urls.login;
  }

  public static getUsers(page: number, size: number): string {
    return this.getApiAddress() + this._urls.users + this._queryEncoder + this._urls.page +`${page}` +
      this._queryUnion + this._urls.per_page +`${size}`;
  }

  public static getResources(): string {
    return this.getApiAddress() + this._urls.resources;
  }
}
