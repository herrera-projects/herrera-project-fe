import {Injectable, Injector} from '@angular/core';

import {Observable} from 'rxjs';
import {HttpService} from "../../../shared/services/http.service";
import {LoginRequest} from "./request/login.request";
import {HttpConstants} from "../../../shared/constants/http.constants";


@Injectable()
export class LoginHttpService extends HttpService {

  constructor(private _injector: Injector) {
    super();
  }

  public doPost(request: LoginRequest): Observable<object> {
    const url = HttpConstants.getUrl();
    return this.httpClient().post(url, request);
  }

  protected injector(): Injector {
    return this._injector;
  }
}
