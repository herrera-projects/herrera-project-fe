import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import {SessionRoutingModule} from "./routing/session-routing.module";
import { LoginMainComponent } from './login-main/login-main.component';
import {SharedModule} from "../shared/shared.module";
import {LoginHttpService} from "./services/login-http.service";

@NgModule({
  declarations: [LoginComponent, LoginMainComponent],
  imports: [
    SessionRoutingModule,
    SharedModule
  ],
  providers: [
    LoginHttpService
  ]
})
export class SessionModule { }
