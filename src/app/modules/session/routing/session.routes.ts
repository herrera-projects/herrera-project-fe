import {Routes} from '@angular/router';
import {LoginComponent} from "../login/login.component";
import {LoginMainComponent} from "../login-main/login-main.component";

export const SESSION_ROUTES: Routes = [
  {
    path: '',
    component: LoginMainComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: '',
        redirectTo: '/session/login',
        pathMatch: 'full'
      }
    ]
  }
];
