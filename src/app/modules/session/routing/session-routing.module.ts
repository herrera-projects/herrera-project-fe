import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SESSION_ROUTES} from "./session.routes";

@NgModule({
  imports: [RouterModule.forChild(SESSION_ROUTES)],
  exports: [RouterModule]
})
export class SessionRoutingModule { }
