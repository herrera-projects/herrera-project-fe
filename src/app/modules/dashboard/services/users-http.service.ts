import {Injectable, Injector} from '@angular/core';

import {Observable} from 'rxjs';
import {HttpConstants} from "../../../shared/constants/http.constants";
import {HttpService} from "../../../shared/services/http.service";

@Injectable()
export class UsersHttpService extends HttpService {

  constructor(private _injector: Injector) {
    super();
  }

  public getUsersList(page: number, size: number): Observable<object> {
    const url = HttpConstants.getUsers(page, size);
    return this.httpClient().get(url);
  }

  public getResourcesList(page: number, size: number): Observable<object> {
    const url = HttpConstants.getResources();
    return this.httpClient().get(url);
  }

  protected injector(): Injector {
    return this._injector;
  }
}
