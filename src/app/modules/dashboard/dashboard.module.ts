import { NgModule } from '@angular/core';
import {DashboardRoutingModule} from "./routing/dashboard-routing.module";
import {UserListComponent} from "./user-list/user-list.component";
import {SharedModule} from "../shared/shared.module";
import { UserMainComponent } from './user-main/user-main.component';
import { UserNavigationComponent } from './user-navigation/user-navigation.component';
import { UserListItemComponent } from './user-list-item/user-list-item.component';
import {UsersHttpService} from "./services/users-http.service";
import {UsersModel} from "./models/users.model";
import { UserResourcesComponent } from './user-resources/user-resources.component';
import { UserResourcesItemComponent } from './user-resources-item/user-resources-item.component';
import {ResourcesModel} from "./models/resources.model";

@NgModule({
  declarations: [UserListComponent, UserMainComponent, UserNavigationComponent, UserListItemComponent, UserResourcesComponent, UserResourcesItemComponent],
  imports: [
    DashboardRoutingModule,
    SharedModule
  ],
  providers: [
    UsersHttpService,
    UsersModel,
    ResourcesModel
  ]
})
export class DashboardModule { }
