import {Component, Input, OnInit} from '@angular/core';
import {User} from "../models/user";

@Component({
  selector: 'app-user-list-item',
  templateUrl: './user-list-item.component.html',
  styleUrls: ['./user-list-item.component.scss']
})
export class UserListItemComponent {

  @Input() public user: User;
  @Input() public index: number;

  private readonly EMPTY = '';

  constructor() {
    this.user = {
      avatar: this.EMPTY,
      email: this.EMPTY,
      last_name: this.EMPTY,
      first_name: this.EMPTY,
      id: this.EMPTY
    };

    this.index = 0;
  }

}
