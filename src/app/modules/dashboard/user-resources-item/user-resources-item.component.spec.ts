import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserResourcesItemComponent } from './user-resources-item.component';

describe('UserResourcesItemComponent', () => {
  let component: UserResourcesItemComponent;
  let fixture: ComponentFixture<UserResourcesItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserResourcesItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserResourcesItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
