import {Component, Input, OnInit} from '@angular/core';
import {Resource} from "../models/resource";

@Component({
  selector: 'app-user-resources-item',
  templateUrl: './user-resources-item.component.html',
  styleUrls: ['./user-resources-item.component.scss']
})
export class UserResourcesItemComponent {

  @Input() public resource: Resource;
  @Input() public index: number;

  private readonly EMPTY = '';

  constructor() {
    this.resource = {
      name: this.EMPTY,
      year: this.EMPTY,
      color: this.EMPTY,
      pantone_value: this.EMPTY,
      id: this.EMPTY
    };

    this.index = 0;
  }

}
