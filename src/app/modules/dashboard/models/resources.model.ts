import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {User} from "./user";
import {Resource} from "./resource";


@Injectable()
export class ResourcesModel extends BehaviorSubject<Resource[]> {

  public resources: Resource[];

  private _page: number;

  constructor() {
    super([]);
    this.resources = [];

    this._page = 1;
  }

  public init(): void {
    this.resources = [];
    this._page = 1;

    this.next([]);
  }

  public addResources(resources: Resource[]): void {
    for (const resource of resources) {
      const found = this.resources.find(
        (userStored: Resource) => {
          return userStored.id === resource.id;
        }
      );

      if (!found) {
        this._pushResource(resource);
      }

    }
    this.next(this.resources);
  }

  private _pushResource(resource: Resource): void {
    this.resources.push(resource);
  }

  public get page(): number {
    return this._page;
  }

  public set page(page: number) {
    this._page = page;
  }
}
