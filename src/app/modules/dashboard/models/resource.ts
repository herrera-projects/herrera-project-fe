export interface Resource {
  id: string;
  name: string;
  year: string;
  color: string;
  pantone_value: string;
}
