import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {User} from "./user";


@Injectable()
export class UsersModel extends BehaviorSubject<User[]> {

  public users: User[];

  private _page: number;

  constructor() {
    super([]);
    this.users = [];

    this._page = 1;
  }

  public init(): void {
    this.users = [];
    this._page = 1;

    this.next([]);
  }

  public addUsers(users: User[]): void {
    for (const user of users) {
      const found = this.users.find(
        (userStored: User) => {
          return userStored.id === user.id;
        }
      );

      if (!found) {
        this._pushUser(user);
      }

    }
    this.next(this.users);
  }

  private _pushUser(user: User): void {
    this.users.push(user);
  }

  public get page(): number {
    return this._page;
  }

  public set page(page: number) {
    this._page = page;
  }
}
