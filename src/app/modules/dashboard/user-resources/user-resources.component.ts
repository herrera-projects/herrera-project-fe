import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs/index";
import {UsersHttpService} from "../services/users-http.service";
import {Resource} from "../models/resource";
import {ResourcesModel} from "../models/resources.model";

@Component({
  selector: 'app-user-resources',
  templateUrl: './user-resources.component.html',
  styleUrls: ['./user-resources.component.scss']
})
export class UserResourcesComponent implements OnInit, OnDestroy {

  public resources: Resource[];

  public readonly size = 3;

  private resourcesModelSubscription: Subscription;
  private resourcesSubscription: Subscription;

  constructor(private usersHttpService: UsersHttpService,
              private resourcesModel: ResourcesModel) {
  }

  ngOnInit() {
    this.loadResources(this.resourcesModel.page, this.size);

    this.resourcesModelSubscription = this.resourcesModel.asObservable().subscribe(
      (resources: Resource[]) => {
        this.resources = resources;
      }
    );
  }

  ngOnDestroy(): void {
    this.resourcesModel.init();

    this._unsubscribe(this.resourcesSubscription);
    this._unsubscribe(this.resourcesModelSubscription);
  }

  public loadResources(page: number, size = this.size): void {
    this.resourcesSubscription = this.usersHttpService.getResourcesList(page, size).subscribe(
      (response: any) => {
        this.resourcesModel.page++;

        const resources = response.data;
        this.resourcesModel.addResources(resources);
      }
    );
  }

  public onScroll(): void {
    this.loadResources(this.resourcesModel.page);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

}
