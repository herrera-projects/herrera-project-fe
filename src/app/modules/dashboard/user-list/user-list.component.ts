import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from "../models/user";
import {UsersHttpService} from "../services/users-http.service";
import {Subscription} from "rxjs/index";
import {UsersModel} from "../models/users.model";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy {

  public users: User[];

  public readonly size = 3;

  private lobbyUserModelSubscription: Subscription;
  private loadUsersSubscription: Subscription;

  constructor(private loadUsersHttpService: UsersHttpService,
              private usersModel: UsersModel) {
  }

  ngOnInit() {
    this.loadUsers(this.usersModel.page, this.size);

    this.lobbyUserModelSubscription = this.usersModel.asObservable().subscribe(
      (users: User[]) => {
        this.users = users;
      }
    );
  }

  ngOnDestroy(): void {
    this.usersModel.init();

    this._unsubscribe(this.loadUsersSubscription);
    this._unsubscribe(this.lobbyUserModelSubscription);
  }

  public loadUsers(page: number, size = this.size): void {
    this.loadUsersSubscription = this.loadUsersHttpService.getUsersList(page, size).subscribe(
      (response: any) => {
        this.usersModel.page++;

        const users = response.data;
        this.usersModel.addUsers(users);
      }
    );
  }

  public onScroll(): void {
    this.loadUsers(this.usersModel.page);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

}
