import {Routes} from '@angular/router';
import {UserListComponent} from "../user-list/user-list.component";
import {UserMainComponent} from "../user-main/user-main.component";
import {UserResourcesComponent} from "../user-resources/user-resources.component";


export const DASHBOARD_ROUTES: Routes = [
  {
    path: '',
    component: UserMainComponent,
    children: [
      {
        path: 'user-list',
        component: UserListComponent
      },
      {
        path: 'resources',
        component: UserResourcesComponent
      },
      {
        path: '',
        redirectTo: '/users/user-list',
        pathMatch: 'full'
      }
    ]
  }
];
